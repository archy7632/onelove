import React, {Component} from "react";
import Input from "./Input.jsx";
import "./AddUser.scss"
import * as css from "./AddUser.scss.json";

import { inject, observer } from 'mobx-react'
import { observable } from "mobx";

import {Redirect} from 'react-router-dom'

@inject("userStore")
@observer
class AddUser extends Component {
  constructor(props) {
    super(props)
    this.state = {
      first: '',
      last: '',
      age: '',
      successSubmit: false,
    }
  }

  handleInputChange = (name, value) => this.setState({ 
    [name]: value 
  })

  addUser = () => this.props.userStore.addUser({
    first: this.state.first,
    last: this.state.last,
    age: this.state.age,
  })

  handleSubmit = event => {
    event.preventDefault()
    event.stopPropagation()
    this.addUser()

    this.setState({ successSubmit: true })
  } 

  render() {
    const {
      first,
      last,
      age,
      successSubmit
    } = this.state

    if(successSubmit) {
      return <Redirect to="/" />
    }

    return (
      <form className={css.form} onSubmit={this.handleSubmit}>
        <Input 
          onChange={this.handleInputChange}
          label="First Name"
          placeholder="First Name"
          pattern="^[a-zA-Z]*$"
          value={this.state.first}
          name='first'
        />

        <Input 
          onChange={this.handleInputChange}
          label="Last Name"
          placeholder="Last Name"
          pattern="^[a-zA-Z]*$"
          value={last}
          name='last'
        />
        
        <Input 
          onChange={this.handleInputChange}
          label="Age"
          placeholder="Age"
          pattern="^[0-9]*$"
          value={age}
          name='age'
        />

        <button className={css.button} type="submit">Add User</button>
      </form> 
    )
  }
}

export default AddUser
