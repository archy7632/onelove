import React, { Component } from "react";
import "./AddUser.scss";
import * as css from "./AddUser.scss.json";

import { inject, observer } from 'mobx-react'

@observer
class Input extends Component {

  handleChange = (event) => {
    this.props.onChange(event.target.name, event.target.value)
  }
  
  render() { 
    const {
      label,
      placeholder,
      pattern,
      value
    } = this.props;
    
    return ( 
      <label className={css.label}>
        {label}
        <input 
          className={css.input} 
          value={value} 
          placeholder={placeholder}
          pattern={pattern}
          onChange={this.handleChange}
          name={this.props.name}
          required
        />
      </label>
     )
  }
}
 
export default Input;