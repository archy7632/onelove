import React, {PureComponent} from 'react'
import TableRow from './TableRow.jsx'
import TableHead from './TableHead.jsx'
import './Table.scss'
import * as css from './Table.scss.json'

import {observer} from "mobx-react"

const Table = observer(({
  dataSource,
  columns
}) => <table className={css.table} cellSpacing="0">
        <TableHead columns={columns} />
        <tbody>
          {dataSource.map((rowData, index) => 
            <TableRow
              rowData={rowData}
              columns={columns}
              key={index}
            />
          )}
        </tbody>
      </table>
)

export default Table
