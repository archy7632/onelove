import React from 'react'
// почему это класс, а не пюрфункция?
import './TableCell.scss'
import * as css from './TableCell.scss.json'

const TableCell = ({
  content
}) => <td className={css.cell}> {content} </td>

export default TableCell
