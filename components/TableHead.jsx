import React from 'react'
import './TableHead.scss'
import * as css from './TableHead.scss.json'

const TableHead = ({
  columns
}) => <thead>
        <tr className={css.row}>
          {columns.map((column, index) =>
            <th className={css.headCell} key={index}>
              {column.title}
            </th>
          )}
        </tr>
      </thead>

export default TableHead
