import React from 'react'
import TableCell from './TableCell.jsx'
import './TableRow.scss'
import * as css from './TableRow.scss.json'

const TableRow = ({
  columns,
  rowData
}) => {

  let content

  return (
    <tr className={css.row}>
      {columns.map((column, index)=> {
        const {render, dataIndex} = column

        if(render) {
          content = render(rowData.guid)
        } else {
          content = rowData[column.dataIndex]
        }
        
        return <TableCell content={content} key={index}/>
        })
      }
    </tr>
  )
}

export default TableRow
