import React from 'react'
import Table from '../components/Table.jsx'
import { observable } from "mobx"
import { inject, observer } from 'mobx-react'

import AddUser from "../components/AddUser.jsx";
import "./App.scss";
import * as css from "./App.scss.json";

import EditUser from "../components/EditUser.jsx";

import {
  Route,
  Link,
  Switch,
  withRouter
} from 'react-router-dom'


@withRouter
@inject("userStore")
@observer
class App extends React.Component {
  
  componentWillMount() {
    this.props.userStore.loadUsers()
  }

  render() {
    const usersColumns = [
      {
        title: '#',
        dataIndex: 'id',
        key: 'id'
      },
      {
        title: 'First Name',
        dataIndex: 'first',
        key: 'first'
      },
      {
        title: 'Last Name',
        dataIndex: 'last',
        key: 'last'
      },
      {
        title: 'Age',
        dataIndex: 'age',
        key: 'age'
      },
      {
        title: 'Action',
        key: 'action',
        render: (guid, css) => (
          <div>
            <Link to="/edituser" onClick={()=> this.props.userStore.editUserGuid = guid}>Edit</Link>
            {` | `}
            <a href="/#" onClick={() => this.props.userStore.removeUser(guid)}>Delete</a>
          </div>
        )
      }
    ]
    const {parsedUsers} = this.props.userStore
    return (
        <React.Fragment>
          <nav className={css.nav}>
            <Link className={css.navLink} to="/">Home</Link>
            <Link className={css.navLink} to="/adduser">AddUser...</Link>
          </nav>

          <Switch>
            <Route 
              path="/" 
              exact 
              render={props => <Table dataSource={parsedUsers} columns={usersColumns} />}
            />
            <Route path="/adduser" component={AddUser}/>
            <Route path="/edituser" component={EditUser}/>
          </Switch>
        </React.Fragment>
    )
  }
}

export default App
