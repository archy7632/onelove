import "babel-polyfill"
import { AppContainer } from "react-hot-loader" // AppContainer is a necessary wrapper component for HMR
import React from "react"
import ReactDOM from "react-dom"
import App from "./containers/App.jsx"
import { Provider } from "mobx-react"

import userStore from "./stores/userStore"

import { BrowserRouter } from 'react-router-dom'

const stores = {
  userStore
}

const render = (Component) => {
  ReactDOM.render(
    <BrowserRouter>
      <Provider {...stores}>
        <AppContainer>
          <Component/>
        </AppContainer>
      </Provider>
    </BrowserRouter>, document.getElementById("app")
  );
}

render(App)

// Hot Module Replacement
if (module.hot) {
  module.hot.accept("./containers/App.jsx", () => {
    render(App)
  });
}
