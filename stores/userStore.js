import { decorate, observable, action, configure, runInAction, computed, } from 'mobx';

// configure({enforceActions: true})

class UserStore {
  parsedUsers = [];

  editUserGuid = ''

  loadUsers = async () => {
    const response = await fetch('/mates')

    const users = await response.json()

    // runInAction ???
    this.parseUsers(users)
  }

  addUser = (arg) => {
    this.parsedUsers.push({
      id: this.parsedUsers.length+1,
      first: arg.first,
      last: arg.last,
      age: arg.age,
      guid: this.uuidv4()
    })
  }

  updateUser = (arg) => {
    this.parsedUsers.map(user => {
      if (user.guid == this.editUserGuid) {
        user.first = arg.first,
        user.last = arg.last,
        user.age = arg.age
      }
    })
  }

  removeUser(guid) {
    let filteredUsers = this.parsedUsers.filter(obj => obj.guid !== guid)
    this.parsedUsers = filteredUsers
  }

  get userData() {
    let userData
    
    this.parsedUsers.map(user => {
      if (user.guid == this.editUserGuid) {
        userData = user
      }
    })

    return userData
  }

  uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    )
  }

  parseUsers = users => {
    users.map((item, index) =>
      this.parsedUsers.push({
        id: index+1,
        first: item.name.first,
        last: item.name.last,
        age: item.age,
        guid: item.guid
      })
    )
  }
}

decorate(UserStore, {
  userData: computed,
  parsedUsers: observable,
  loadUsers: action,
  parseUsers: action,
  removeUser: action,
  addUser: action,
})

export default new UserStore();