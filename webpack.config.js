const path = require('path');
const bodyParser = require('body-parser');

module.exports = {
  mode: 'development',
  entry: [ 'babel-polyfill', './index.jsx'],
  output: {
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 8080,
    setup(app) {
      app.get('/adduser', (req, res) => res.sendFile(path.join(__dirname, '/dist/index.html')))
      app.get('/edituser', (req, res) => res.sendFile(path.join(__dirname, '/dist/index.html')))
      app.get("/mates", bodyParser.json(), function(req, res) {
        console.log(req.body);
        res.send([
          {
            "guid": "10e49a2b-0910-49a4-94aa-f29311fd0f79",
            "age": 37,
            "name": {
              "first": "Flowers",
              "last": "Hatfield"
            },
            "email": "flowers.hatfield@undefined.us"
          },
          {
            "guid": "69827b93-1b7a-4475-9b27-5caffdf5257e",
            "age": 32,
            "name": {
              "first": "Whitney",
              "last": "Pope"
            },
            "email": "whitney.pope@undefined.net"
          },
          {
            "guid": "0f3808f9-6d86-4b1f-b305-7c17a30f2a63",
            "age": 34,
            "name": {
              "first": "Pacheco",
              "last": "Gilmore"
            },
            "email": "pacheco.gilmore@undefined.io"
          },
          {
            "guid": "c718963d-cd57-489b-a885-54f70bb44caf",
            "age": 24,
            "name": {
              "first": "Eloise",
              "last": "Wade"
            },
            "email": "eloise.wade@undefined.co.uk"
          },
        ])
      });
    },
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: {
          loader: 'babel-loader'
        },
        exclude: [/node_modules/],
      },
      {
        test: /\.css$/,
        use: [ 
          {loader:'style-loader'},
          {loader:'css-loader',},
          {loader:'postcss-loader'}
        ],
      },
      {
        test: /\.scss$/,
        use: [ 
          {loader:'style-loader'},
          {loader:'css-loader'},
          {loader:'postcss-loader'},
          {loader:'sass-loader'}
        ]
      },
    ]
  },
};
